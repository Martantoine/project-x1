# parse md to html
1. process headings
2. process line
3. process italic
4. process bold
5. process strikethrough
6. process highlight

# process italic, bold, strikethrough & highlight
i := first special character encoutered from the start
for i = 0 to line.length - 1:
    if line[i] == special_character
        if state == state::start
            start = i
            state = state::end
        else
            end = i
            state = state::start
        if end - start > 1
            couple.push_back(start, end)
        else
            start = end
            state = state::end

i := first special character encoutered from the start
count = 0
for i = 0 to line.length - 1:
    if line[i] == special_character
        count++
        if count == 2
            if state == state::start
                start = i
                state = state::end
            else
                end = i
                state = state::start
            if end - start > 1
                couple.push_back(start, end)
            else
                start = end
                state = state::end
    else
        count = 0