#include "file.hpp"

using namespace std;

bool openFile(string path, fstream& file)
{
	file.open(path, fstream::in | fstream::out);	
	if(file.is_open())
		return true;
	return false;
}

bool readFile(fstream& file, vector<string>& content)
{
	string line;
	//check has already be done in openFile(), but it's safer to check
	if(file.is_open())
	{
		while (getline(file, line))
			content.push_back(line);
		return true;
	}
	return false;
}
