#include <string>
#include <vector>
#include <fstream>

bool openFile(std::string path, std::fstream& file);
bool readFile(std::fstream& file, std::vector<std::string>& content);
