#include "md_parser.hpp"
#include "file.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char** argv)
{   
    if(argc != 2)
        return -1;

	fstream file;
	vector<string> md_text;
	if(!openFile(argv[1], file))
	{
		cout << "ERROR::Failed to open " << argv[1] << endl;
		return -1;
	}
	if(!readFile(file, md_text))
	{
		cout << "ERROR::File closed while trying to read it " << argv[1] << endl;
		return -1;
	}

    cout << md_parse(md_text) << endl;
	file.close();

    return 0;
}
