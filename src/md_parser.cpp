#include "md_parser.hpp"
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

string md_parse(vector<string>& input)
{
	string output = "<!DOCTYPE html>\n"
                    "<html>\n"
                    "    <head>\n"
                    "        <meta charset=\"utf-8\">\n"
                	"    </head>\n"
                    "    <body>\n";

	string line;
    for(int i(0); i < input.size(); i++)
    {
		line = input[i];
        proccessHeading(line);
        proccessItalicBold(line);
		//Add all the other proccess

		output += line + "</br>\n"; //end of line
    }
    output += 	"   </body>\n"
    			"</html>";
    return output;
}

void proccessHeading(string& line)
{
    short count(0), start(0);
    enum class SEARCH_STATE { FIRST, END };
    SEARCH_STATE state = SEARCH_STATE::FIRST;
    for(short i(0); i < line.length(); i++)
    {
        if(line[i] == '#')
        {
            if(state == SEARCH_STATE::FIRST)
            {
                start = i;
                count++;
                state = SEARCH_STATE::END;
            }
            else
                count++;
        }
        else if(count > 0) break;
    }
    if(count != 0)
        line = line.replace(0, count, "<h" + to_string(count) + ">");
}

void proccessItalicBold(string& line)
{
    short start(0), end(0);
    vector<short> italics, bolds;
    enum class SEARCH_STATE { FIRST, END, END2 };
    SEARCH_STATE state = SEARCH_STATE::FIRST;
	bool bold_likely = false;
    for(short i(0); i < line.length(); i++)
    {
        if(line[i] == '*' || line[i] == '_')
        {
            if(state == SEARCH_STATE::FIRST)
            {
                start = i;
                state = SEARCH_STATE::END;
            }
            else if(state == SEARCH_STATE::END)
            {
                end = i;
                //** with no space in between must not be considered as in italic	
                if(end - start > 1 && !bold_likely)
                {
                    state = SEARCH_STATE::FIRST;
                    italics.push_back(start);
                    italics.push_back(end);
					bold_likely = false;
                }
				else if(!bold_likely)
					bold_likely = true;
				else
					state = SEARCH_STATE::END2;
            }
			else if(state == SEARCH_STATE::END2)
			{
				if(i - end == 1) //a bold case
				{
					bolds.push_back(start);
					bolds.push_back(end);
					bold_likely = false;
				}
				else //just a regular italic case
				{
					italics.push_back(start);
					italics.push_back(end);
					i--;
					bold_likely = false;
				}
				state = SEARCH_STATE::FIRST;
			}
        }
    }
	if(bold_likely)
	{
		italics.push_back(start);
		italics.push_back(end);
	}
	//Function that interpret correctly *word**anotherword**word*
	for(short i(2); italics.size() >= 6 && 2*i < italics.size(); i++)
	{
		if((abs(italics[2*(i-2)+1] - italics[2*(i-2)+2]) == 1) && 
		   (abs(italics[2*(i-1)+1] - italics[2*(i-1)+2]) == 1))
		{
			bolds.push_back(italics[2*(i-2)+1]);
			bolds.push_back(italics[2*(i-1)+1]);
			italics.erase(italics.begin()+2*(i-2)+1, italics.begin()+2*(i-2)+5);
		}
	} 
	
	//Sorting bolds is mandatory because of the previous push_back(s)
	sort(bolds.begin(), bolds.end());

    for(short i(0); 2*i < static_cast<short>(italics.size()); i++)
    {
		//Substituing '*' or '_' in italic case by their html equivalent
		//the +7*i offset the italic array element being after 2*i one
        line.replace(italics[2*i]+7*i, 1, "<em>");
        line.replace(italics[2*i+1]+7*i+3, 1, "</em>");
	
		//Offset the bolds array element being after the italics[2*i] or italics[2*i+1]
		for(short j(0); 2*j < static_cast<short>(bolds.size()); j++)
		{
			if(bolds[2*j] > italics[2*i+1]+7*i)
			{
				bolds[2*j] += 7;
				bolds[2*j+1] += 7;
			}
			else if(bolds[2*j] > italics[2*i]+7*i)
			{
				bolds[2*j] += 3;
				bolds[2*j+1] += 3;
			}
		}
	}
	for(short i(0); 2*i < static_cast<short>(bolds.size()); i++)
	{
		line.replace(bolds[2*i]+3*i, 2, "<b>");
		line.replace(bolds[2*i+1]+3*i+1, 2, "</b>"); 
	}
#ifdef DEBUG
	for(int i(0); i<italics.size();i++) cout << italics[i] << "::";
	cout << endl;
	for(int i(0); i<bolds.size();i++) cout << bolds[i] << "::";
#endif
}
