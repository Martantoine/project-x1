#ifndef MD_PARSER
#define MD_PARSER

#include <string>
#include <vector>

/*The only function that should be used outside of this module
 *the index of the array corresponds to the line number
 */
std::string md_parse(std::vector<std::string>& input);

//Do not call the following functions outside of this module
void proccessHeading 			(std::string& line);
void proccessItalicBold 		(std::string& line);
/*
void proccessStrikethrough 		(std::string& line){}
void proccessHighlight 			(std::string& line){}
void proccessOrderedList 		(std::string& line){}
void proccessUnorderedList 		(std::string& line){}
*/

#endif /* ifndef MD_PARSER */
